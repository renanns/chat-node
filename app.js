const app = require('./server');
require('dotenv').config();

const server = require('http').createServer(app);
const io = require('socket.io')(server);

app.set(io);
app.set('io', io);

server.listen(process.env.PORT_SERVER, () => {console.log(`Server running in ${process.env.PORT_SERVER}`)});

io.on('connection', (socket) => {
    console.log(`Socket connected: ${socket.id}`);

    // Quando um usuário se desconecta, remove-o do registro de usuários conectados
    socket.on('disconnect', () => {
        console.log(`Socket disconnected: ${socket.id}`);
    });

    // Escuta o método send_message_server recebendo os dados e logo após emitindo uma menssagem para todos as conexões no server.
    socket.on('send_message_server', (data) => {
        socket.emit('send_message_client', data);
        socket.broadcast.emit('send_message_client', data);
    });
});

