const validationUser = (req, res, next) => {
    const {username, password} = req.body;

    if(username == undefined || username == '') {
        return res.status(400).render('login', {isInvalid: true});
    };

    if (password == undefined || password == '') {
        return res.status(400).render('login', {isInvalid: true});
    };

    next();
};

module.exports = {
    validationUser,
}