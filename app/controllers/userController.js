const userModel = require('../models/userModel')

const login = (req, res) => {
    return res.render('login');
};

const chat = async (req, res) => {
    const {username, password} = req.body;
    const io = req.app.get('io'); // acessando a váriavel io configurada no servidor.

    try {
        const user = await userModel.checkUser(username, password);
        
        if (user) {
            io.emit('user_connected', {user: username});
            return res.render('chat', { user: user.name });
        } else {
            return res.render('login');
        };
    } catch (error) {
        return res.status(500).send('Erro interno do servidor', error);
    };
};


module.exports = {
    login,
    chat
};