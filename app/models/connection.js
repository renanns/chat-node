const { Pool } = require('pg');
require('dotenv').config();

const pool = new Pool({
  user: process.env.USER_DB,
  host: process.env.HOST,
  database: process.env.DB,
  password: process.env.PASS,
  port: process.env.PORT_DB,
});

module.exports = pool;

