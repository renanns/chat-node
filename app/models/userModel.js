const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const checkUser = async (username, password) => {
    try {
        const user = await prisma.user.findFirst({
            where: {
                name: username, 
                pass: password
            }
        });
        return user;
    } catch (error) {
        throw error;
    };
};

module.exports = {
    checkUser,
};