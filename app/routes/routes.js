const express = require('express');
const router = express.Router();

const controller = require('../controllers/userController');
const middleware = require('../middlewares/userMiddleware')

router.get('/login', controller.login);
router.post('/chat', middleware.validationUser, controller.chat);

module.exports = router;